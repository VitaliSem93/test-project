﻿using System;
using System.Collections.Generic;
using Orders;
using Auxiliary;

namespace Test
{
    /// <summary>
    /// Analyzes a file with data. Adds orders to OrderBook and deletes orders from OrderBook.
    /// Finds an average price of orders. Determines the greatest price of orders from OrderBook.
    /// </summary>
    /// <namespace name = Orders>Classes Order and OrderBook</namespace>
    /// <namespace name = Methods>Static methods GetInfo, StringToInt, StringToDouble and GetAveragePrice</namespace>
    class Program
    {
        static void Main()
        {
            string info = Methods.GetInfo();
            string[] lines = info.Split('\n');
            OrderBook orderBook = new OrderBook();
            List<int> timeStamps = new List<int>();
            List<double> prices = new List<double>();
            for (int i = 0; i < lines.Length; i++)
            {
                int TimeStamp = Methods.StringToInt(lines[i].Substring(0, lines[i].IndexOf(' ')), "timeStamp", i);
                try
                {
                    if (lines[i].Substring(lines[i].IndexOf(' ') + 1, 1) == "I")
                    {
                        int ID = Methods.StringToInt(lines[i].Substring(lines[i].IndexOf('I') + 2, lines[i].LastIndexOf(' ') - (lines[i].IndexOf('I') + 2)), "ID", i);
                        double Price = Methods.StringToDouble(lines[i].Substring(lines[i].LastIndexOf(' ')), "price", i);
                        timeStamps.Add(TimeStamp);
                        prices.Add(Price);
                        Order order = new Order(TimeStamp, ID, Price);
                        orderBook.AddNewOrder(order, ID, TimeStamp, Price);
                    }
                    else if (lines[i].Substring(lines[i].IndexOf(' ') + 1, 1) == "E")
                    {
                        int ID = Methods.StringToInt(lines[i].Substring(lines[i].IndexOf('E') + 2), "ID", i);
                        try
                        {
                            prices.Add(orderBook.RemoveOrder(ID, lines, i));
                            timeStamps.Add(TimeStamp);
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine($"Error: {ex.Message}");
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                    continue;
                }
            }

            double averagePrice = Methods.GetAveragePrice(timeStamps, prices);
            Console.WriteLine($"The average price of the current session is {averagePrice}$.");
            try
            {
                double maxPrice = orderBook.FindMaxPrice();
                Console.WriteLine($"The greatest price from the OrderBook is {maxPrice}$.");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("The OrderBook is empty.");
            }
        }
    }
}
