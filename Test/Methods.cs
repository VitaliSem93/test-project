﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace Auxiliary
{
    /// <summary>
    /// Auxiliary methods for the test project.
    /// </summary>
    /// <method name = GetInfo>Takes the path to the file and returns its conten</method>
    /// <method name = StringToInt>Converts string value to Int32 value</method>
    /// <method name = StringToDouble>Converts string value to Double value</method>
    /// <method name = GetAveragePrice>Returns an average price of the session</method>
    static class Methods
    {
        public static string GetInfo()
        {
            Console.WriteLine(@"Enter the path to the file in format ""D:\TestFile.txt"":");
            string info = string.Empty;
            string path = Console.ReadLine();
            FileInfo fileInfo = new FileInfo(@path);
            try
            {
                if (!fileInfo.Exists)
                {
                    throw new NullReferenceException(nameof(fileInfo));
                }

                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    info = sr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(info))
                {
                    throw new ArgumentNullException(nameof(info), "The file is empty.");
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("File wasn't found. Please check the path.");
                info = GetInfo();
            }

            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return info;
        }

        public static int StringToInt(string value, string name, int i)
        {
            if (!int.TryParse(value, out int result))
            {
                throw new ArgumentException($"Some problems with {name} value for the {i + 1} line of the list.");
            }

            return result;
        }

        public static double StringToDouble(string value, string name, int i)
        {
            if (!double.TryParse(value, out double result))
            {
                throw new ArgumentException($"Some problems with {name} value for the {i + 1} line of the list.");
            }

            return result;
        }

        public static double GetAveragePrice(List<int> timeStamps, List<double> prices)
        {
            int[] TimeStamps = timeStamps.ToArray();
            double[] Prices = prices.ToArray();
            double sum = 0.0;
            int period = 0;
            for (int i = 1; i < Prices.Length; i++)
            {
                double temp = Prices[i - 1];
                if (Prices[i] != Prices[i - 1])
                {
                    if (period == 0)
                    {
                        period += TimeStamps[i] - TimeStamps[i - 1];
                    }

                    sum += temp * period;
                    period = 0;
                }
                else
                {
                    period += TimeStamps[i] - TimeStamps[i - 1];
                }
            }
            if (Prices[Prices.Length - 1] == Prices[Prices.Length - 2])
            {
                sum += Prices[Prices.Length - 1] * period;
            }
            else
            {
                sum += Prices[Prices.Length - 1] * (TimeStamps[TimeStamps.Length - 1] - TimeStamps[TimeStamps.Length - 2]);
            }
            double averagePrice = sum / (TimeStamps[TimeStamps.Length - 1] - TimeStamps[0]);
            return averagePrice;
        }
    }
}
