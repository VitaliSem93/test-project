﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auxiliary;

namespace Orders
{
    /// <summary>
    /// Orders namespace contains Order class and OrderBook class.
    /// </summary>
    /// <method name = AddNewOrder>Adds new order to the OrderBook</method>
    /// <method name = RemoveOrder>Removes specified order from the OrderBook</method>
    /// <method name = FindMaxPrice>Return the greatest price of the orders from the OrderBook</method>
    class Order
    {
        public int TimeStamp;
        public int ID;
        public double Price;

        public Order(int timestamp, int id, double price)
        {
            TimeStamp = timestamp;
            ID = id;
            Price = price;
        }
    }

    class OrderBook
    {
        private readonly List<Order> orders;

        public OrderBook()
        {
            orders = new List<Order>();
        }

        public void AddNewOrder(Order order, int ID, int timeStamp, double price)
        {
            orders.Add(order);
            Console.WriteLine($"The order was added: ID - {ID}, timeStamp - {timeStamp}, price - {price}.");
        }

        public double RemoveOrder(int id, string[] lines, int i)
        {
            double Price = 0.0;
            int index = Array.FindIndex(orders.ToArray(), x => x.ID == id);
            if (index == -1)
            {
                throw new ArgumentException($"There is no order with the specified ID-{id}.");
            }
            else
            {
                orders.RemoveAt(index);
                foreach (string s in lines)
                {
                    if (s.IndexOf($"I {id}") != -1)
                    {
                        Price = Methods.StringToDouble(s.Substring(s.LastIndexOf(' ')), "price", i);
                        break;

                    }
                }
                Console.WriteLine($"The order with ID-{id} was removed.");
            }

            return Price;
        }

        public double FindMaxPrice()
        {
            if (orders.Count == 0)
            {
                throw new ArgumentNullException(nameof(orders));
            }
            else
            {
                double maxPrice = orders.Max(x => x.Price);
                return maxPrice;
            }
        }
    }
}
